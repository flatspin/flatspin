# flatspin

flatspin is a GPU-accelerated simulator for systems of interacting nanomagnet
spins arranged on a 2D lattice, also known as Artificial Spin Ice (ASI).
flatspin can simulate the dynamics of large ASI systems with thousands of
interacting elements.

flatspin is written in Python and uses OpenCL for GPU acceleration.
flatspin is open-source software and released under a GNU GPL license

## Installing

Install and update using [pip](https://pip.pypa.io/en/stable/getting-started):

```sh
pip install -U flatspin
```

## Getting started

Once installed, head over to the [user manual](https://flatspin.gitlab.io) to get started.

## Links

- Website: <https://flatspin.gitlab.io>
- Documentation: <https://flatspin.gitlab.io>
- Releases: <https://pypi.org/project/flatspin>
- Code: <https://gitlab.com/flatspin/flatspin>
- Issue tracker: <https://gitlab.com/flatspin/flatspin/-/issues>
