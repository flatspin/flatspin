(userguide)=

# User guide

Welcome to the flatspin user guide!

flatspin is a GPU-accelerated simulator for systems of interacting nanomagnet
spins arranged on a 2D lattice, also known as Artificial Spin Ice (ASI).
Through GPU acceleration, flatspin can simulate the dynamics of a large number
of magnets within practical time frames.  flatspin is written in Python and
utilizes OpenCL to accelerate calculations on the GPU.

The best way to get familiar with flatspin, is to play with the simulator in an
interactive Python environment. This guide is written as a [Jupyter
notebook](https://jupyter.org/), which you can download and run yourself.
Each of the chapters can be downloaded by clicking the download link at the top
of the page.

## Topics

The topics covered in this user guide are fairly self-contained, meaning it is
possible to read them in any order.  However, getting familiar with the
[theory](theory) is an encouraged starting point.

```{tableofcontents}
```
