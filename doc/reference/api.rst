.. _api:

API reference
=============

.. module:: flatspin

Submodules
----------
.. toctree::
   :titlesonly:
   :maxdepth: 1
   :glob:

   api/*
