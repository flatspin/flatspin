:mod:`flatspin.vertices`
=======================

.. module:: flatspin.vertices

.. automodule:: flatspin.vertices
    :members:
    :undoc-members:
