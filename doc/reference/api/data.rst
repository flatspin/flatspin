:mod:`flatspin.data`
=======================

.. module:: flatspin.data

.. automodule:: flatspin.data
    :members:
    :undoc-members:
