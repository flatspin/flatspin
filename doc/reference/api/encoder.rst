:mod:`flatspin.encoder`
=======================

.. module:: flatspin.encoder

.. automodule:: flatspin.encoder
    :members:
    :undoc-members:
