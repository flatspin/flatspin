:mod:`flatspin.astroid`
=======================

.. module:: flatspin.astroid

.. automodule:: flatspin.astroid
    :members:
    :undoc-members:
