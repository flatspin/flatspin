:mod:`flatspin.model`
=====================

.. module:: flatspin.model

.. automodule:: flatspin.model
   :members:
   :undoc-members:
   :private-members: _init_geometry, _vertex_size
