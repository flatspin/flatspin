# Examples

Here you can find a list of examples on how flatspin can be used.

The examples take the form of [Jupyter notebooks](https://jupyter.org/), and can be downloaded by clicking the download button at the top of each page.

```{tableofcontents}
```
